package com.thingple.message;

/**
 * 签到消息
 * Created by lism on 2017/8/4.
 */

public class MQTTMessage {

    /**
     * topic
     */
    public String topic;

    /**
     * 消息内容
     */
    public String content;
}
